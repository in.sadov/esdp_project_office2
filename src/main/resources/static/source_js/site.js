let post1 = JSON.parse(localStorage.getItem('post1'));


const createPost = (post) => {
    return `
        <div class="card user-card" id="${post.id}">
        <div class="card-header">
            <p>Задача: <b>${post.id}</b>
            <a href="#" class="removePost" id="${post.id}">X</a>
        </div>
        <div class="card-body">
            <h5 class="card-title">${post.post}</h5>
            <a href="#" class="user-btn inProgress">${post.status}</a>
        </div>
        </div>
        `
}


const initPosts = () => {
    if(post1 === null){
        post1 = [];
    }else{
        for (let i = 0; i < post1.length; i++) {
            if(post1[i].status == "In-Progress>>"){
                const postElement = createPost(post1[i]);
                document.getElementById('post1').innerHTML += postElement;
            }else if(post1[i].status === "Done>>"){
                const postElement = createPost(post1[i]);
                document.getElementById('post2').innerHTML += postElement;
            }else if(post1[i].status === "delete"){
                const postElement = createPost(post1[i]);
                document.getElementById('post3').innerHTML += postElement;
            }
        }
    }
}


initPosts();


const addPost = (e) => {
    e.preventDefault();
    let form = e.target;
    console.log(form);
    const formData = new FormData(form);
    let data = Object.fromEntries(formData);
    console.log(data);
    const id = post1.length + 1;
    const post = {
        id: id,
        status: "In-Progress>>",
        post: data['post']
    };
    post1.push(post);
    localStorage.setItem('post1', JSON.stringify(post1));
    let postElement = createPost(post);
    document.getElementById("post1").innerHTML += postElement;
}

$("#add-post-form").on('submit', addPost);

$(document).on('click', '.inProgress', (e) => {
    let id = e.target.parentElement.parentElement.id;
    console.log(id);
    for (let i = 0; i < post1.length; i++) {
        if(post1[i].id == id && post1[i].status == "In-Progress>>"){
            document.getElementById(id).remove();
            post1[i].status = "Done>>";
            localStorage.setItem('post1', JSON.stringify(post1));
            newPosts(post1);
            inProgressPosts(post1);
            donePosts(post1);
        }else  if (post1[i].id == id &&  post1[i].status == "Done>>") {
            document.getElementById(id).remove();
            post1[i].status = "delete";
            localStorage.setItem('post1', JSON.stringify(post1));
            newPosts(post1);
            inProgressPosts(post1);
            donePosts(post1);
        }else if (post1[i].id == id &&  post1[i].status == "delete"){
            post1.splice(i, 1);
            document.getElementById(id).remove();
            localStorage.setItem('post1', JSON.stringify(post1));
            newPosts(post1);
            inProgressPosts(post1);
            donePosts(post1);
        }
    }
});

$(document).on('click', '.removePost', deletePostX);

function deletePost(e){
    let id = e.target.parentElement.parentElement.id;
    for (let i = 0; i < post1.length; i++) {
        if(post1[i].id == id){
            post1.splice(i, 1);
            document.getElementById(id).remove();
            localStorage.setItem('post1', JSON.stringify(post1));
        }
    }
}

function deletePostX(e){
    let id = e.target.parentElement.parentElement.parentElement.id;
    for (let i = 0; i < post1.length; i++) {
        if(post1[i].id == id){
            post1.splice(i, 1);
            document.getElementById(id).remove();
            localStorage.setItem('post1', JSON.stringify(post1));
        }
    }
}


function newPosts(posts){
    let div = "";
    if (posts.length > 0) {
        for (let i = 0; i < posts.length; i++) {
            if (posts[i].status === 'In-Progress>>') {
                const postElement = createPost(posts[i])
                div += postElement + " ";
            }
        }
        document.getElementById('post1').innerHTML = div;
    }
}

function inProgressPosts(posts = []){
    let div = "";
    if (posts.length > 0) {
        for (let i = 0; i < posts.length; i++) {
            if (posts[i].status === 'Done>>') {
                const postElement = createPost(posts[i])
                div += postElement + " ";
            }
        }
        document.getElementById('post2').innerHTML = div;
    }
}

function donePosts(posts = []){
    let div = "";
    if (posts.length > 0) {
        for (let i = 0; i < posts.length; i++) {
            if (posts[i].status === 'delete') {
                const postElement = createPost(posts[i])
                div += postElement + " ";
            }
        }
        document.getElementById('post3').innerHTML = div;
    }
}

// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}