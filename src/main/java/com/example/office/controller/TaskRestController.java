package com.example.office.controller;

import com.example.office.service.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;
@AllArgsConstructor
@RestController
public class TaskRestController {
    private final TaskService taskService;
}
