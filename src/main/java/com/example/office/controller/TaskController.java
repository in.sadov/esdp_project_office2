package com.example.office.controller;

import com.example.office.service.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;

@Controller
@AllArgsConstructor
public class TaskController {
    private final TaskService taskService;
}
