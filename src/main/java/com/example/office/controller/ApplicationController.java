package com.example.office.controller;

import com.example.office.service.ApplicationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;

@Controller
@AllArgsConstructor
public class ApplicationController {
    private final ApplicationService applicationService;
}
