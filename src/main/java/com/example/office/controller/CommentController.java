package com.example.office.controller;

import com.example.office.service.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;

@Controller
@AllArgsConstructor
public class CommentController {
    private final CommentService commentService;
}
