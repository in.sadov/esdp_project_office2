package com.example.office.controller;

import com.example.office.service.ProjectService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;

@Controller
@AllArgsConstructor
public class ProjectController {
    private final ProjectService projectService;

}
