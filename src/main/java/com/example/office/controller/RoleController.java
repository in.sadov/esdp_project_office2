package com.example.office.controller;

import com.example.office.service.*;
import lombok.*;
import org.springframework.stereotype.Controller;

@Controller
@AllArgsConstructor
public class RoleController {
    private final RoleService roleService;
}