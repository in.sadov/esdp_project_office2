package com.example.office.DTO;

import com.example.office.model.Comment;
import com.example.office.model.User;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class CommentDTO {
    private Integer id;
    private String text;
    private long time = System.currentTimeMillis();//время вплость до милисикунд, для лучше сортировки комментариев
    private LocalDate data;
    private User author;

    public static CommentDTO from(Comment comment){
        return CommentDTO.builder()
                .id(comment.getId())
                .author(comment.getAuthor())
                .text(comment.getText())
                .time(comment.getTime())
                .data(comment.getData())
                .build();
    }
}
