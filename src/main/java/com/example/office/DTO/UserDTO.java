package com.example.office.DTO;

import com.example.office.model.*;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
public class UserDTO {

    private String name;

    private String surname;

    private String username;

    private String email;

    private String password;

    private String iin;

    private int role_id;

    private int position_id;

    private boolean isActive;

    public static UserDTO from(User user) {
        return UserDTO.builder()
                .name(user.getName())
                .surname(user.getSurname())
                .username(user.getUsername())
                .email(user.getEmail())
                .password(user.getPassword())
                .iin(user.getIin())
                .role_id(user.getRole().getId())
                .position_id(user.getPosition().getId())
                .isActive(user.isActive())
                .build();
    }


}