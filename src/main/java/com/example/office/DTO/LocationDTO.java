package com.example.office.DTO;

import com.example.office.model.Location;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
public class LocationDTO {

    private String name;

    private String address;

    private String contact_name;

    private String phone_number;

    private String iin;

    public static LocationDTO from(Location location) {
        return LocationDTO.builder()
                .name(location.getName())
                .address(location.getAddress())
                .contact_name(location.getContact_name())
                .phone_number(location.getPhone_number())
                .iin(location.getIin())
                .build();
    }
}