package com.example.office.DTO;

import com.example.office.model.Position;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
public class PositionDTO {

    private String name;

    public static PositionDTO from(Position position) {
        return PositionDTO.builder()
                .name(position.getName())
                .build();
    }
}