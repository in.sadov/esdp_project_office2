package com.example.office.DTO;

import com.example.office.model.*;
import lombok.*;
import org.springframework.format.datetime.DateFormatter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(access = AccessLevel.PACKAGE)
public class TaskDTO {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Integer id;
        @NotBlank
        @Size(min = 1, max = 128)
        private String name;
        @NotBlank
        @Size(min = 1, max = 128)
        private String description;
        private ApplicationDTO application;
        private UserDTO executor;
        private LocalDateTime createDate;
        private LocalDateTime closingDate;
        private DateFormatter dueDate;
        private StatusDTO status;
        private PriorityDTO priority;
        private LocationDTO location;
        private List<Comment> comment;



 static TaskDTO from (Task task){
return builder()
        .id(task.getId())
        .name(task.getName())
        .description(task.getDescription())
        .application(ApplicationDTO.from(task.getApplication()))
        .description(task.getDescription())
        .executor(UserDTO.from(task.getExecutor()))
        .closingDate(task.getClosingDate())
        .createDate(task.getCreateDate())
        .dueDate(task.getDueDate())
        .status(StatusDTO.from(task.getStatus()))
        .priority(PriorityDTO.from(task.getPriority()))
        .build();
 }
}
