package com.example.office.DTO;

import com.example.office.model.Priority;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
public class PriorityDTO {

    private String name;

    public static PriorityDTO from(Priority priority) {
        return PriorityDTO.builder()
                .name(priority.getName())
                .build();
    }
}