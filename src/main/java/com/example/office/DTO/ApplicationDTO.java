package com.example.office.DTO;

import com.example.office.model.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@Builder
public class ApplicationDTO {
    private String description;
    private UserDTO responsible;
    private ProjectDTO project;
    private LocalDate createDate;
    private LocalDate endDate;
    private LocalDate dueDate;
    private StatusDTO status;
    private UserDTO author;
    private Double price;
    private LocalDate planStartDate;
    private LocalDate factStartDate;
    private TypeDTO type;
    private LocationDTO location;
    private List<Comment> comment;

    public static ApplicationDTO from(Application application){
        return ApplicationDTO.builder()
                .description(application.getDescription())
                .responsible(UserDTO.from(application.getResponsible()))
                .project(ProjectDTO.from(application.getProject()))
                .createDate(application.getCreateDate())
                .endDate(application.getEndDate())
                .dueDate(application.getDueDate())
                .status(StatusDTO.from(application.getStatus()))
                .author(UserDTO.from(application.getAuthor()))
                .price(application.getPrice())
                .planStartDate(application.getPlanStartDate())
                .factStartDate(application.getFactStartDate())
                .type(TypeDTO.from(application.getType()))
                .location(LocationDTO.from(application.getLocation()))
                .comment(application.getComment())
                .build();
    }
}
