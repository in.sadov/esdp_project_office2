package com.example.office.DTO;

import com.example.office.model.*;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProjectDTO {

    private Integer id;
    private String name;
    private User author;
    private User responsible;
    private LocalDate dateCreate;
    private LocalDate datePlannedComplete;
    private LocalDate dateFactComplete;
    private Location location;
    private Status status;
    private List<Comment> comment;

    public static ProjectDTO from(Project project){
      return ProjectDTO.builder()
                .id(project.getId())
                .name(project.getName())
                .author(project.getAuthor())
                .responsible(project.getResponsible())
                .dateCreate(project.getDateCreate())
                .datePlannedComplete(project.getDatePlannedComplete())
                .dateFactComplete(project.getDateFactComplete())
                .location(project.getLocation())
                .status(project.getStatus())
                .comment(project.getComment())
                .build();
    }
}
