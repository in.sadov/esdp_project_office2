package com.example.office.DTO;

import com.example.office.model.Role;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
public class RoleDTO {

    private String name;

    public static RoleDTO from(Role role) {
        return RoleDTO.builder()
                .name(role.getName())
                .build();
    }
}