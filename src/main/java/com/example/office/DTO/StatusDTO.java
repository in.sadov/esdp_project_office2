package com.example.office.DTO;

import com.example.office.model.Status;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
public class StatusDTO {

    private String name;

    public static StatusDTO from(Status status) {
        return StatusDTO.builder()
                .name(status.getName())
                .build();
    }
}