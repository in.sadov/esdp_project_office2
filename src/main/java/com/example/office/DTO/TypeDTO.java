package com.example.office.DTO;

import com.example.office.model.Type;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
public class TypeDTO {

    private String name;

    public static TypeDTO from(Type type) {
        return TypeDTO.builder()
                .name(type.getName())
                .build();
    }
}