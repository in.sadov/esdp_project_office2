package com.example.office.model;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
public class User {

    private Integer id;

    private String name;

    private String surname;

    private String username;

    private String email;

    private String password;

    private String iin;

    private Role role;

    private Position position;

    private boolean isActive;
}