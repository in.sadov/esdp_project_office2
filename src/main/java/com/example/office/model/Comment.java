package com.example.office.model;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class Comment {
    private Integer id;
    private String text;
    private long time = System.currentTimeMillis();//время вплость до милисикунд, для лучше сортировки комментариев
    private LocalDate data; //для отображения даты к комментарии
    private User author;
}
