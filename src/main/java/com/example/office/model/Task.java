package com.example.office.model;

import lombok.*;

import org.springframework.format.datetime.DateFormatter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;


@Getter
@Setter
//@Table(name="tasks")
//@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class Task {

    private Integer id;

    private String name;

    private String description;

    private Application application;

    private User executor;

    private LocalDateTime createDate;

    private LocalDateTime closingDate;

    private DateFormatter dueDate;

    private Status status;

    private Priority priority;

    private Location location;
    private List<Comment> comment;

}
