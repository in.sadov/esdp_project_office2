package com.example.office.model;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
public class Location {

    private Integer id;

    private String name;

    private String address;

    private String contact_name;

    private String phone_number;

    private String iin;
}