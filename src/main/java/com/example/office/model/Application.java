package com.example.office.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@Builder
public class Application {
    private Integer id;
    private String description;
    private User responsible;
    private Project project;

    @Builder.Default
    private LocalDate createDate = LocalDate.now();
    private LocalDate endDate;
    private LocalDate dueDate;
    private Status status;
    private User author;
    private Double price;
    private LocalDate planStartDate;
    private LocalDate factStartDate;
    private Type type;
    private Location location;
    private List<Comment> comment;
}
