package com.example.office.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
//@Table(name = "projects")
//@Entity
public class Project {

    @Id
    private Integer id;
    private String name;
    private User author;
    private User responsible;
    private LocalDate dateCreate;
    private LocalDate datePlannedComplete;
    private LocalDate dateFactComplete;
    private Location location;
    private Status status;
    private List<Comment> comment;

}
