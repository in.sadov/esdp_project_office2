package com.example.office.repository;

import com.example.office.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRepository extends JpaRepository<Status, Integer> {
    Status findByName(String name);

    void deleteByName(String name);
}