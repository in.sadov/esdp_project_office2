package com.example.office.repository;

import com.example.office.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends JpaRepository<Location, Integer> {
    Location findByName(String name);

    Location findByAddress(String address);

    Location findByContactName(String contactName);

    Location findByPhoneNumber(String phoneNumber);

    Location findByIin(String iin);

    void deleteByName(String name);

    void deleteByAddress(String address);

    void deleteByContactName(String contactName);

    void deleteByIin(String iin);
}