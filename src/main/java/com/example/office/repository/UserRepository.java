package com.example.office.repository;

import com.example.office.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByEmail(String email);

    User findByUsername(String username);

    User findByName(String name);

    User findBySurname(String surname);

    User findByIin(String iin);

    void deleteByName(String name);

    void deleteBySurname(String surname);

    void deleteByUsername(String username);

    void deleteByEmail(String email);

    void deleteByIin(String iin);
}