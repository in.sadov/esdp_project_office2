package com.example.office.repository;

import com.example.office.model.Priority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PriorityRepository extends JpaRepository<Priority, Integer> {
    Priority findByName(String name);

    void deleteByName(String name);
}