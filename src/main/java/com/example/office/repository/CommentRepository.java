package com.example.office.repository;

import com.example.office.model.Comment;
import com.example.office.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CommentRepository extends JpaRepository<Comment,Integer> {
    Optional<Comment> findAllByAuthor(User user);
    void deleteCommentByAuthor(User user);
}
