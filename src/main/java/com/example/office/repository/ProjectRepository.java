package com.example.office.repository;

import com.example.office.model.Project;
import com.example.office.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {
    Optional<Project> findAllByAuthor(User user);
}
