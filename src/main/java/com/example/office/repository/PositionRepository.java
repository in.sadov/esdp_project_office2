package com.example.office.repository;

import com.example.office.model.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionRepository extends JpaRepository<Position, Integer> {
    Position findByName(String name);

    void deleteByName(String name);
}