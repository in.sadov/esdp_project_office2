package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.exception.*;
import com.example.office.model.*;
import com.example.office.repository.*;
import lombok.*;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class LocationService {
    private final LocationRepository locationRepository;

    public List<Location> getAll() {
        return locationRepository.findAll();
    }

    public Location getById(int id) {
        return locationRepository.findById(id)
                .orElseThrow(() -> new LocationNotFoundException("Объект с данным id не найден."));
    }

    public Location getByName(String name) {
        return locationRepository.findByName(name);
    }

    public Location getByAddress(String address) {
        return locationRepository.findByAddress(address);
    }

    public Location getByContactName(String contactName) {
        return locationRepository.findByContactName(contactName);
    }

    public Location getByPhoneNumber(String phoneNumber) {
        return locationRepository.findByPhoneNumber(phoneNumber);
    }

    public Location getByIin(String iin) {
        return locationRepository.findByIin(iin);
    }

    public void create(LocationDTO locationDTO) {
        Location location = Location
                .builder()
                .name(locationDTO.getName())
                .address(locationDTO.getAddress())
                .contact_name(locationDTO.getContact_name())
                .phone_number(locationDTO.getPhone_number())
                .iin(locationDTO.getIin())
                .build();

        locationRepository.save(location);
    }

    public void deleteById(int id) {
        locationRepository.deleteById(id);
    }

    public void deleteByName(String name) {
        locationRepository.deleteByName(name);
    }

    public void deleteByAddress(String address) {
        locationRepository.deleteByAddress(address);
    }

    public void deleteByContactName(String contactName) {
        locationRepository.deleteByContactName(contactName);
    }

    public void deleteByIin(String iin) {
        locationRepository.deleteByIin(iin);
    }
}