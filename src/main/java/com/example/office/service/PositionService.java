package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.*;
import com.example.office.repository.*;
import lombok.*;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class PositionService {
    private PositionRepository positionRepository;

    public List<Position> getAll() {
        return positionRepository.findAll();
    }

    public Position getById(int id) {
        return positionRepository.findById(id).orElseThrow();
    }

    public Position getByName(String name) {
        return positionRepository.findByName(name);
    }

    public void create(PositionDTO positionDTO) {
        Position position = Position
                .builder()
                .name(positionDTO.getName())
                .build();

        positionRepository.save(position);
    }

    public void deleteById(int id) {
        positionRepository.deleteById(id);
    }

    public void deleteByName(String name) {
        positionRepository.deleteByName(name);
    }
}