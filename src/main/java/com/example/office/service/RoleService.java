package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.*;
import com.example.office.repository.*;
import lombok.*;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class RoleService {
    private final RoleRepository roleRepository;

    public List<Role> getAll() {
        return roleRepository.findAll();
    }

    public Role getById(int id) {
        return roleRepository.findById(id).orElseThrow();
    }

    public Role getByName(String name) {
        return roleRepository.findByName(name);
    }

    public void create(RoleDTO roleDTO) {
        Role role = Role
                .builder()
                .name(roleDTO.getName())
                .build();

        roleRepository.save(role);
    }

    public void deleteById(int id) {
        roleRepository.deleteById(id);
    }

    public void deleteByName(String name) {
        roleRepository.deleteByName(name);
    }
}