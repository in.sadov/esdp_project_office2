package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.*;
import com.example.office.repository.*;
import lombok.*;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class PriorityService {
    private final PriorityRepository priorityRepository;

    public List<Priority> getAll() {
        return priorityRepository.findAll();
    }

    public Priority getById(int id) {
        return priorityRepository.findById(id).orElseThrow();
    }

    public Priority getByName(String name) {
        return priorityRepository.findByName(name);
    }

    public void create(PriorityDTO priorityDTO) {
        Priority priority = Priority
                .builder()
                .name(priorityDTO.getName())
                .build();

        priorityRepository.save(priority);
    }

    public void deleteById(int id) {
        priorityRepository.deleteById(id);
    }

    public void deleteByName(String name) {
        priorityRepository.deleteByName(name);
    }
}