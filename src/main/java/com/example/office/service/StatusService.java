package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.*;
import com.example.office.repository.*;
import lombok.*;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class StatusService {
    private final StatusRepository statusRepository;

    public List<Status> getAll() {
        return statusRepository.findAll();
    }

    public Status getById(int id) {
        return statusRepository.findById(id).orElseThrow();
    }

    public Status getByName(String name) {
        return statusRepository.findByName(name);
    }

    public void create(StatusDTO statusDTO) {
        Status status = Status
                .builder()
                .name(statusDTO.getName())
                .build();

        statusRepository.save(status);
    }

    public void deleteById(int id) {
        statusRepository.deleteById(id);
    }

    public void deleteByName(String name) {
        statusRepository.deleteByName(name);
    }
}