package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.exception.UserNotFoundException;
import com.example.office.model.*;
import com.example.office.repository.*;
import lombok.*;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final RoleService roleService;
    private final PositionService positionService;

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public User getById(int id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("Пользователь с данным id не найден."));
    }

    public User getByName(String name) {
        return userRepository.findByName(name);
    }

    public User getBySurname(String surname) {
        return userRepository.findBySurname(surname);
    }

    public User getByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public User getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User getByIin(String iin) {
        return userRepository.findByIin(iin);
    }

    public void create(UserDTO userDTO) {
        Role role = roleService.getById(userDTO.getRole_id());

        Position position = positionService.getById(userDTO.getPosition_id());

        User user = User
                .builder()
                .name(userDTO.getName())
                .surname(userDTO.getName())
                .username(userDTO.getUsername())
                .email(userDTO.getEmail())
                .password(userDTO.getPassword())
                .iin(userDTO.getIin())
                .role(role)
                .position(position)
                .isActive(userDTO.isActive())
                .build();

        userRepository.save(user);
    }

    public void deleteById(int id) {
        userRepository.deleteById(id);
    }

    public void deleteByName(String name) {
        userRepository.deleteByName(name);
    }

    public void deleteBySurname(String surname) {
        userRepository.deleteBySurname(surname);
    }

    public void deleteByUsername(String username) {
        userRepository.deleteByUsername(username);
    }

    public void deleteByEmail(String email) {
        userRepository.deleteByEmail(email);
    }

    public void deleteByIin(String iin) {
        userRepository.deleteByIin(iin);
    }
}