package com.example.office.service;

import com.example.office.DTO.*;
import com.example.office.model.*;
import com.example.office.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class TypeService {
    private final TypeRepository typeRepository;

    public List<Type> getAll() {
        return typeRepository.findAll();
    }

    public Type getById(int id) {
        return typeRepository.findById(id).orElseThrow();
    }

    public Type getByName(String name) {
        return typeRepository.findByName(name);
    }

    public void create(TypeDTO typeDTO) {
        Type type = Type
                .builder()
                .name(typeDTO.getName())
                .build();

        typeRepository.save(type);
    }

    public void deleteById(int id) {
        typeRepository.deleteById(id);
    }

    public void deleteByName(String name) {
        typeRepository.deleteByName(name);
    }
}