package com.example.office.service;

import com.example.office.DTO.ApplicationDTO;
import com.example.office.model.Application;
import com.example.office.repository.ApplicationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ApplicationService {
    private final ApplicationRepository applicationRepository;

    public ApplicationDTO findOne(Integer id) {
        return ApplicationDTO.from(applicationRepository.findById(id).orElseThrow());
    }

    public List<Application> findAll(){
        return applicationRepository.findAll();
    }

}
